# Selenium WebDriver TestNG Framework- Run under Docker Containers with the Help of Grid
Automating functional UI and end to end tests for https://www.onemap.gov.sg website. This project uses the page object model (POM) design. Parallel execution will start if "selenium-grid=Yes" in "Configuration.properties" file, else it will run locally.

## Stack & Libraries
- Java
- Selenium WebDriver
- TestNG 
- Maven
- Extent Reports

## Prerequisites
- IDE (Eclipse)
- JDK (version 17 or above ) with configuration
- Maven configuration
- Docker Desktop Installer for HUB & node Distributer
- wsl_update_x64 for Shell script command  

### Easy way to configure Maven (Window)
<details>
  <summary>Click to view instructions</summary> 
  
1. Download Maven (apache-maven-3.9.6-bin.zip for window) from [here](https://maven.apache.org/download.cgi)
2. Move the downloaded apache-maven-3.9.6 folder to your Home directory (This is the folder with your username)Example;"C:\Users\ashim"

1. Set system variables:
M2_HOME=C:\Users\ashim\apache-maven-3.9.6
MAVEN_HOME=C:\Users\ashim\apache-maven-3.9.6
```
2. Append the Maven bin folder to the path:
```
PATH=C:\Users\ashim\apache-maven-3.9.6\bin
```
mvn -version
```
</details>

### Easy way to configure java(JDK) (Window)
<details>
  <summary>Click to view instructions</summary> 
  
1. Download JDK  (bin.exe for window) from [here](https://www.oracle.com/in/java/technologies/downloads/)
2.Go to download folder click on .exe file and install

1. Set system variables:
JAVA_HOME=C:\Program Files\Java\jdk-17
```
2. Append the Maven bin folder to the path:
```
PATH=C:\Program Files\Java\jdk-17
```
java -version
```
</details>

## Project components
- Page objects are in the directory *src/main/java/pages*
- Test classes are in the the directory *src/test/java/tests*
- Listener class is in the directory *src/main/java/util*
- Driver configuration file in /Resources*
- Extent report file in /report*
- TestNG Report file in /test-output/emailable-report.html

## How to run tests
In Command Prompt:
```
git clone https://gitlab.com/ashim.nayak2/testng_maven_automationframwork.git
```
Change directory to the location of cloned project folder and run Maven clean
```
mvn clean
```
To run the test
```
mvn test

## How to Containerized tests
1. Run "Docker Desktop" form Start menu
```
2. Open Windows Powershell for Selenium Grid HUB/Node settings & Run.
  - Run The Command one by one :
  - docker network create grid
  
  - docker run -d -p 4444-4444:4444-4444 --net grid --name selenium-hubAshim selenium/hub:4.17.0-20240123
  
  - docker run -d --net grid -e SE_EVENT_BUS_HOST=selenium-hub `
    --shm-size="2g" `
    -e SE_EVENT_BUS_PUBLISH_PORT=4442 `
    -e SE_EVENT_BUS_SUBSCRIBE_PORT=4443 `
    selenium/node-chrome:4.17.0-20240123
    
  - docker run -d --net grid -e SE_EVENT_BUS_HOST=selenium-hub `
    --shm-size="2g" `
    -e SE_EVENT_BUS_PUBLISH_PORT=4442 `
    -e SE_EVENT_BUS_SUBSCRIBE_PORT=4443 `
    selenium/node-edge:4.17.0-20240123
    
  - docker run -d --net grid -e SE_EVENT_BUS_HOST=selenium-hub `
    --shm-size="2g" `
    -e SE_EVENT_BUS_PUBLISH_PORT=4442 `
    -e SE_EVENT_BUS_SUBSCRIBE_PORT=4443 `
    selenium/node-firefox:4.17.0-20240123
```  
  -Like
    ![TestResult](./ReadMeScreenShot/PowerShellComand.png)

# Check "Docker Desktop" window for running Containers
  
  - Should be Like
    ![TestResult](./ReadMeScreenShot/RunningContainers.png)
    
# Check grid url "http://localhost:4444/ui" 
  
  - Should be Like
    ![TestResult](./ReadMeScreenShot/SeleniumGridInstancesOverview.png)
    
  - No session started 
    ![TestResult](./ReadMeScreenShot/BeforeSessionStart.png)

# Trigger "mvn clean test" in command prompt form Project location

  - Should be Like
    ![TestResult](./ReadMeScreenShot/MavenTrigger.png)
 
# Grid Overview window After test start 

  - Should be Like
    ![TestResult](./ReadMeScreenShot/AfterMavenOverview.png)  
     
# Grid Session window After test start 

  - Should be Like
    ![TestResult](./ReadMeScreenShot/AfterMavenSessions.png) 
    
# Grid Session Live View
  
  - Default password for VNC Should be "secret" 
    ![TestResult](./ReadMeScreenShot/LiveViewAccess.png)
    
  -VNC live execution
    ![TestResult](./ReadMeScreenShot/LiveView.png)
 
# Command prompt log
  
  - Should be Like
    ![TestResult](./ReadMeScreenShot/cmdLog.png)




## Highlights
- This framework supports Chrome, Edge, Forefox browsers
- Screenshot on test failure: A screenshot of the active browser is captured and stored in the screenshots folder
- Extent reporting and logging: After the test finishes, a visual report is generated for all the executed test cases from the suite. This report can be found in the 'report' folder
- Extent reporting One
![TestResult](./ReadMeScreenShot/ExtentReport1.png)
- Extent reporting Two
![TestResult](./ReadMeScreenShot/ExtentReport2.png)
- TestNG reporting
![TestResult](./ReadMeScreenShot/TestNGReport.png)


