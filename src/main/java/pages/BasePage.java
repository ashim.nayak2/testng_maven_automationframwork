package pages;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.net.URL;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import io.github.bonigarcia.wdm.WebDriverManager;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import util.PropertyReader;

public class BasePage {

	public static WebDriver driver;
	
	public static ExtentTest logger;
	public static ExtentReports report;

	@Parameters ("BrowserType")
	@BeforeClass(alwaysRun=true)
	public void setup(ITestContext context,String sBrowserType) {

		String selenium_grid=new PropertyReader().readProperty("selenium-grid");
		String hubURL=new PropertyReader().readProperty("hubURL");
		String URL=new PropertyReader().readProperty("URL");
     	switch(sBrowserType) {
     	case "safari":	
    		SafariOptions sopt = new SafariOptions();
    	    
    		if (selenium_grid.equals("Yes")) {
    			System.out.println("About to Start new Safari RemoteWebDriver");
    			try {
    				driver=new RemoteWebDriver(new URL(hubURL), sopt);
    				driver.manage().window().maximize();
    				driver.get(URL);
    			}catch(MalformedURLException e) {
    				e.printStackTrace();
    			}		
    		} else {
    		driver = new SafariDriver(sopt);
    	    driver.manage().window().maximize();
    	    driver.get(URL);
    		context.setAttribute("WebDriver", driver);
    		break;}
    		
		case "chrome":
			
		ChromeOptions options = new ChromeOptions();
		Map<String, Object> prefs = new HashMap<String, Object>();

		prefs.put("download.default_directory", "./Download");
		prefs.put("profile.default_content_settings.geolocation", 1);
		options.setExperimentalOption("prefs", prefs);
        options.addArguments("--disable-notifications");
		options.setExperimentalOption("prefs", prefs);
//		options.addArguments("headless");
		if (selenium_grid.equals("Yes")) {
			System.out.println("About to Start new chrome RemoteWebDriver");
			try {
				driver=new RemoteWebDriver(new URL(hubURL), options);
				driver.manage().window().maximize();
				driver.get(URL);
			}catch(MalformedURLException e) {
				e.printStackTrace();
			}		
		} else {
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.get(URL);
		context.setAttribute("WebDriver", driver);}
		break;
		
	case "firefox":
		
		FirefoxOptions fopt = new FirefoxOptions();
//		fopt.setCapability("marionette", true);
		if (selenium_grid.equals("Yes")) {
			System.out.println("About to Start new firefox RemoteWebDriver");
			try {
				driver=new RemoteWebDriver(new URL(hubURL), fopt);
				driver.manage().window().maximize();
				driver.get(URL);
			}catch(MalformedURLException e) {
				e.printStackTrace();
			}		
		} else {
	    driver = new FirefoxDriver(fopt);
		driver.manage().window().maximize();
		driver.get(URL);
		context.setAttribute("WebDriver", driver);
		break;}
		
	case "edge":	
		EdgeOptions eopt = new EdgeOptions();
	    
		if (selenium_grid.equals("Yes")) {
			System.out.println("About to Start new edge RemoteWebDriver");
			try {
				driver=new RemoteWebDriver(new URL(hubURL), eopt);
				driver.manage().window().maximize();
				driver.get(URL);
			}catch(MalformedURLException e) {
				e.printStackTrace();
			}		
		} else {
		driver = new EdgeDriver(eopt);
	    driver.manage().window().maximize();
	    driver.get(URL);
		context.setAttribute("WebDriver", driver);
		break;}
	
		default:
			System.out.println("no browser");
			break;
		
		}
}


	@AfterClass(alwaysRun=true)
	public void tearDown() {
		driver.quit();
	}
	
	
}
