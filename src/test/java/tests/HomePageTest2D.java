package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;
import pages.BasePage;
import pages.HomePage;

public class HomePageTest2D extends BasePage{

	@Test(priority=0)
	public void verifyTitle() throws InterruptedException {
		HomePage homePage = new HomePage(driver);
		logger.log(LogStatus.INFO, "Checking title matches string");
		Assert.assertEquals(homePage.getTitle(), "OneMap");
		logger.log(LogStatus.PASS, "Title matches with title from DOM");
	}

	@Test(priority=1)
	public void verifyLogo() {
		HomePage homePage = new HomePage(driver);
		logger.log(LogStatus.INFO, "Checking if logo is present");
		Assert.assertTrue(homePage.logoDisplay());
		logger.log(LogStatus.PASS, "Logo is present");
	}

//	@Test(priority=2)
//	public void searchLocation() throws InterruptedException {
//		HomePage homePage = new HomePage(driver);
//		String origin="SINGAPORE GENERAL HOSPITAL";
//		String destination="STATE COURTS";
//
//		homePage.clickRouteIcon();
//		logger.log(LogStatus.INFO, "Click on Route icon");
//		
//		homePage.originInput(origin);
//		logger.log(LogStatus.INFO, "Enter origin Location In Search box");
//		Assert.assertTrue(homePage.checkmarkerInfoboxTxt());
//		logger.log(LogStatus.PASS, "Search origin Result Pop-up Displayed");
//
//		homePage.destinationInput(destination);
//		logger.log(LogStatus.INFO, "Enter destination Location In Search box");
//		Assert.assertTrue(homePage.checkmarkerInfoboxTxt());
//		logger.log(LogStatus.PASS, "Search destination Result Pop-up Displayed");
//
//	}
//	
//	@Test(priority=3)
//	public void verifyCommunity() throws InterruptedException {
//		HomePage homePage = new HomePage(driver);
//		
//		String atriBeforeClick = homePage.checkCommunityAttribute();
//		logger.log(LogStatus.INFO, "Before click on Community, class attribute Value is: "+ atriBeforeClick);
//		homePage.clickCommunity();
//		logger.log(LogStatus.PASS, "Community Option clicked");
//		String atriAfterClick = homePage.checkCommunityAttribute();
//		logger.log(LogStatus.INFO, "After click on Community, class attribute Value is: "+ atriAfterClick);
//		Assert.assertFalse(atriBeforeClick.equalsIgnoreCase(atriAfterClick));
//		logger.log(LogStatus.PASS, "Community Option working as expected");
//	}
}