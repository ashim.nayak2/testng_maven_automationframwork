package testAPI;

import static io.restassured.RestAssured.*;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class BaseAPI {

	
	@BeforeClass
	public void basicAuthSetup() 
	{
		RestAssured.authentication=RestAssured.preemptive().basic("postman", "password");
		RestAssured.baseURI="https://postman-echo.com";	
//		given().auth().oauth("consumerKey", "consumerSecret", "accessToken", "tokenSecret").get("your end point URL");
//		given().auth().oauth2("Access token").get("your end point URL");
	}

	@Test
    public void getData() {
		 //Using the preemptive directive of basic auth to send credentials to the server
       
        Response res = get("/basic-auth");
        ResponseBody body = res.body();
        //Converting the response body to string
        String rbdy = body.asString();
        System.out.println("Data from the GET API- "+rbdy);
    }
}
