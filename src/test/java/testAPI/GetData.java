package testAPI;

import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import io.restassured.response.Response;


public class GetData {

	@Test
	public void testResponcecode() 
	{
//		Response resp=RestAssured.get("https://api.openweathermap.org/data/2.5/weather?lat=44.34&lon=10.99&appid=e31ab3666ec25016e71299229de0e193");
		Response resp=get("https://api.openweathermap.org/data/2.5/weather?lat=44.34&lon=10.99&appid=e31ab3666ec25016e71299229de0e193");	
		int code=resp.getStatusCode();
		System.out.println("Status code:"+code);
		Assert.assertEquals(code, 200);
	}
	
	@Test
	public void testBody() 
	{
		Response resp=get("https://api.openweathermap.org/data/2.5/weather?lat=44.34&lon=10.99&appid=e31ab3666ec25016e71299229de0e193");
		String data=resp.asString();
		System.out.println("Responce Body:"+data);
		System.out.println("Responce Time:"+resp.getTime());
	
	}
	
}
