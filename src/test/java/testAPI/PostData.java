package testAPI;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;



public class PostData {


	@Test
	public void testPost() 
	{

		RequestSpecification request=given();

		request.header("Content-Type", "application/json");

		JSONObject json= new JSONObject();

		json.put("id", "55");
		json.put("title", "Api Post");
		json.put("views", 777);
		json.put("Author", "Ashim");

		request.body(json.toJSONString());


		Response resp=request.post("http://localhost:3000/posts");	
		int code=resp.getStatusCode();
		System.out.println("Status code:"+code);
		Assert.assertEquals(code, 201);


	}
	@Test
	public void testDelete() 
	{

		RequestSpecification request=given();


		Response resp=request.delete("http://localhost:3000/posts/9e45");	
		int code=resp.getStatusCode();
		System.out.println("Status code:"+code);
		Assert.assertEquals(code, 200);


	}
	
	@Test
	public void testPut() 
	{

		RequestSpecification request=given();

		request.header("Content-Type", "application/json");

		JSONObject json= new JSONObject();

		json.put("id", "55");
		json.put("title", "Api Post");
		json.put("views", 777);
		json.put("Author", "Ashim");

		request.body(json.toJSONString());


		Response resp=request.put("http://localhost:3000/posts/55");	
		int code=resp.getStatusCode();
		System.out.println("Status code:"+code);
		Assert.assertEquals(code, 200);


	}

}
